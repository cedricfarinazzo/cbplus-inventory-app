from django.conf.urls import url
from django.urls import path

from . import views

app_name = "app"
urlpatterns = [
    path('product/<int:gtin>/delete', views.ProductDeleteView.as_view(), name="delete"),
    path('product/<int:gtin>/<int:pk>/delete', views.ProductExpirationDeleteView.as_view(), name="delete_date"),
    path('product/<int:gtin>', views.ProductView.as_view(), name="view"),
    url('add', views.ProductAddView.as_view(), name="add"),
    url('', views.IndexView.as_view(), name="index")
]