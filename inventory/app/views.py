from django.shortcuts import get_object_or_404, redirect
from django.views.generic import ListView, View
from django.views.generic.edit import FormView
from .models import Product, ProductExpiration
from .forms import ProductAddForm


# Create your views here.

class IndexView(ListView):
    template_name = "app/index.html"
    model = Product


class ProductAddView(FormView):
    template_name = "app/add.html"
    form_class = ProductAddForm
    success_url = "/"

    # If form is valid
    def form_valid(self, form):
        product = Product.objects.filter(barcode=form.cleaned_data["barcode"])
        if product.exists():
            product = product.first()
            if product.name is not form.cleaned_data["name"]:
                # Update the name of the product
                product.name = form.cleaned_data["name"]
                product.save()
        else:
            # Product does not exist, we create it
            product = Product(name=form.cleaned_data["name"],
                              barcode=form.cleaned_data["barcode"])
            product.save()
        ProductExpiration(product=product, date=form.cleaned_data["expiration_date"]).save()

        return super(ProductAddView, self).form_valid(form)


class ProductView(ListView):
    template_name = "app/product.html"
    model = Product

    def get(self, request, gtin, *args, **kwargs):
        self.object = get_object_or_404(self.model, barcode=gtin)
        return super(ProductView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ProductView, self).get_context_data(**kwargs)
        context['product'] = self.object
        return context

    def get_queryset(self):
        return self.object.expirations.order_by("date").all()


class ProductDeleteView(View):
    redirect_url = 'app:index'
    model = Product

    def get(self, request, gtin):
        product = get_object_or_404(self.model, barcode=gtin)
        product.delete()
        return redirect(self.redirect_url)


class ProductExpirationDeleteView(View):
    redirect_url = 'app:index'
    model = ProductExpiration

    def get(self, request, gtin, pk):
        product_expir = get_object_or_404(self.model, product__barcode=gtin, pk=pk)
        product = product_expir.product
        product_expir.delete()

        if product.expirations.count() == 0:
            # No stock, so we can delete the product
            product.delete()

        return redirect(self.redirect_url)
