from django.db import models
from django.urls import reverse


# Create your models here.

class ProductExpiration(models.Model):
    product = models.ForeignKey("Product",
                                related_name="expirations",
                                on_delete=models.CASCADE)
    date = models.DateField(verbose_name="Product expiration date")

    @property
    def delete_url(self):
        return reverse('app:delete_date', kwargs={'gtin': self.product.barcode, 'pk': self.pk})


class Product(models.Model):
    name = models.CharField(verbose_name="Product name", max_length=256)
    barcode = models.IntegerField(verbose_name="Product GTIN", unique=True)

    @property
    def shortest_expiration_date(self):
        return self.expirations.order_by('date').first()

    @property
    def url(self):
        return reverse('app:view', kwargs={'gtin': self.barcode})

    @property
    def delete_url(self):
        return reverse('app:delete', kwargs={'gtin': self.barcode})
