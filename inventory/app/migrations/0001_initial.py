# Generated by Django 2.2.13 on 2020-06-22 17:16

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256, verbose_name='Product name')),
                ('expiration_date', models.DateTimeField(verbose_name='Product expiration date')),
                ('barcode', models.IntegerField(verbose_name='Product GTIN')),
            ],
        ),
    ]
