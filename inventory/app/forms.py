from django import forms


class ProductAddForm(forms.Form):
    name = forms.CharField(label="product_name", max_length=256)
    barcode = forms.IntegerField(label="product_barcode", min_value=0)
    expiration_date = forms.DateField(label="expiration_date", widget=forms.DateInput(attrs={'type': 'date'}))
