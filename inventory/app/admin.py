from django.contrib import admin

from .models import Product, ProductExpiration


# Register your models here.

class ProductAdmin(admin.ModelAdmin):
    model = Product
    list_display = ("id", "name", "barcode",)
    readonly_fields = ("id",)
    search_fields = ("name", "barcode",)


admin.site.register(Product, ProductAdmin)


class ProductExpirationAdmin(admin.ModelAdmin):
    model = ProductExpiration
    list_display = ("product", "date",)
    search_fields = ("date",)


admin.site.register(ProductExpiration, ProductExpirationAdmin)
