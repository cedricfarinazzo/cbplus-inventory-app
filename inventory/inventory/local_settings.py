from os import environ


def get_bool(name, default=False):
    if not name in environ:
        return default
    val = environ.get(name).lower()
    if val in ["false", "n", "no"]:
        return False
    return True


def get_string(name, default=""):
    return environ.get(name, default)


def get_list(name, separator=""):
    return get_string(name).split(separator)


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = get_bool("DJANGO_DEBUG", False)

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = get_string("DJANGO_SECRET_KEY", "")

ALLOWED_HOSTS = get_list("DJANGO_ALLOWED_HOSTS", " ")

STATIC_ROOT = get_string("DJANGO_STATIC_ROOT", "")

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": get_string("DJANGO_DB_NAME", ""),
        "USER": get_string("DJANGO_DB_USER", ""),
        "PASSWORD": get_string("DJANGO_DB_PASSWORD"),
        "HOST": get_string("DJANGO_DB_HOST"),
        "PORT": get_string("DJANGO_DB_PORT", "5432"),
    }
}
