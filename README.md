# CB+ inventory app

[[_TOC_]]

## Author

- Cédric FARINAZZO <cedric.farinazzo@epita.fr>

## How to run

### Dependencies

You only need a PostgreSQL server.

### Dev mode

In dev mode, you need Python3 to be installed on your system.
Then you can install application dependencies (in a virtualenv preferably):
``python3 -m pip install -r requirements.txt``

Jump into the ``inventory`` directory (``cd inventory``).

#### Environment variables
Set the following environment variables:

- ``DJANGO_DEBUG``: enable debug mode.
    - value: ``True`` or ``False``
    - default: ``False``

- ``DJANGO_ALLOWED_HOSTS``: a list of host/domain names that this app can serve.
    - value: a list of host separated by a space (``"*"`` to allow every hosts)
    - default: ``""``

- ``DJANGO_SECRET_KEY``: the django secret key
    - value: a string

- ``DJANGO_DB_NAME``: the database name
    - value: a string

- ``DJANGO_DB_USER``: the username to connect to the postgresql server
    - value: a string

- ``DJANGO_DB_PASSWORD``: the password to connect to the postgresql server
    - value: a string

- ``DJANGO_DB_HOST``: the ip/domain of the postgresql server
    - value: a string

- ``DJANGO_DB_PORT``: the postgresql server port
    - value: an integer
    - default: ``5432``
    
- ``DJANGO_STATIC_ROOT``: static root for static files (only in poduction mode). Static files will be copied to this directory. 
    - value: a string

After that, you can apply the application migrations.
``python3 manage.py migrate``

Finnaly, you can launch the server in dev mode.
``python3 manage.py runserver``

### Production mode

The production server is built on a docker image automatically using Gitlab's CI/CD (see [.gitlab-ci.yml](./.gitlab-ci.yml)).
The docker image is pushed [here](https://gitlab.com/cedricfarinazzo/cbplus-inventory-app/container_registry/).
Image tag started with ``dev-`` are development images. **Don't use them.**

To run it: 
```
docker run -d -it registry.gitlab.com/cedricfarinazzo/cbplus-inventory-app:3.1
```
You can use the command line arguments ``-e`` or ``--env`` to specify environment variables described [here](#environment-variables) (except ``DJANGO_DEBUG``, ``DJANGO_ALLOWED_HOSTS`` and ``DJANGO_STATIC_ROOT``).

### How it works ?

I choose to create 2 models: one to store the GTIN code (with a name) and another model to store expiration date related to a product.
Using Django related name, it's easy to get the shortest expiration date of a product (see [models.py](./inventory/app/models.py)).

![models](./doc/models.png)

To add product, I use a Django form, it simplifies data validation.

Also, I use class-based views. This prevents code duplication.

To render the HTML page, I use Django template with a layout (see [base.html](./inventory/templates/app/base.html)) that I extend for each page and override the ``content`` block.

### Improvements

- Provide kubernetes configuration.
- Better design
- Functional tests

### Website

This Django application is running at [https://cbplus-test.cedricfarinazzo.cf/](https://cbplus-test.cedricfarinazzo.cf/).

### Thanks

Thank you. If you have any question, contact me at cedric.farinazzo@epita.fr