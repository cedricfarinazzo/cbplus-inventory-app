#!/bin/sh

service nginx start
service nginx status

cd "$DJANGO_WORKDIR"
python3 "$DJANGO_WORKDIR"/manage.py migrate
python3 "$DJANGO_WORKDIR"/manage.py collectstatic --noinput --clear

exec gunicorn -b 0.0.0.0:8000 inventory.wsgi:application
