FROM nginx

RUN mkdir -p /usr/src/app
RUN mkdir -p /usr/src/docker
WORKDIR /usr/src/app

RUN mkdir -p /data/www/app/static

COPY requirements.txt .

# Install python, postgres
RUN apt-get update && apt-get install -y python3 python3-pip postgresql postgresql-server-dev-11

# Install python package
RUN python3 -m pip install --no-cache-dir -r requirements.txt

# Copy the application
COPY inventory .
COPY docker /usr/src/docker
COPY docker/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
ENV DJANGO_WORKDIR="/usr/src/app"
ENV DJANGO_DEBUG=False
ENV DJANGO_ALLOWED_HOSTS="*"
ENV DJANGO_STATIC_ROOT="/data/www/app/static"

CMD [ "/usr/src/docker/entrypoint.sh" ]
